var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var path = require('path');
var PurifyCSS = require('purifycss-webpack-plugin');
var webpack = require('webpack');

module.exports = {
    devtool: 'source-map',
    entry: {
        app: path.resolve(__dirname, '../src/js/script.js'),
        vendor: []
    },
    output: {
        path: path.resolve(__dirname, '../dist/js'),
        filename: 'script.js'
    },
    module: {
        loaders: [
            {
                exclude: /node_modules/,
                loader: 'babel',
                test: /\.jsx?$/
            },
            {
                exclude: /dist/,
                loader: ExtractTextPlugin.extract('style', ['css', 'postcss', 'sass']),
                test: /\.s?[ac]ss$/
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('../css/style.css', {
            allChunks: true
        }),
        new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.js'),
        new PurifyCSS({
            basePath: path.resolve(__dirname, 'src'),
            paths: [
                '../**/*.html',
            ],
            purifyOptions: {
                minify: true,
                info: true,
                rejected: true
            },
            resolveExtensions: [
                '.html',
                '.jade',
                '.php'
            ]
        }),
        new webpack.optimize.UglifyJsPlugin({
            cacheFolder: path.resolve(__dirname, '../.cache'),
            debug: false,
            output: {
                comments: false
            },
            compressor: {
                warnings: false
            }
        })
    ],
    postcss: function() {
        return [
            autoprefixer({
                browsers: ['last 10 versions']
            })
        ];
    },
    sassLoader: {
        includePaths: [
            path.join(__dirname, '.,/src/scss')
        ]
    },
};
